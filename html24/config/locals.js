module.exports = function (app) {
    app.use(function(req, res, next) {
        if(typeof req.app.locals.isLoggedIn) {
            res.locals.isLoggedIn = false;
        }
        next();
    });
};
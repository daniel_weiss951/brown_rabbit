const { baristaCup } = require('../controllers/baristaCup.controller');

module.exports = function(app) {
    app.get('/barista-cup', baristaCup);
}
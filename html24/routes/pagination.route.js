const { page_1, page_2, page_3} = require('../controllers/pagination.controller');

module.exports = function(app) {
    app.get('/page-1', page_1);
    app.get('/page-2', page_2);
    app.get('/page-3', page_3);
}
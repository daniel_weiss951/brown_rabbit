require('dotenv').config();
const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const bodyparser = require('body-parser');
const formidable = require('express-formidable');
const bcryptjs = require('bcryptjs');
const session = require('express-session');


app.use(bodyparser.json());
app.use(formidable());

app.use(cookieParser('keyboar cat'));

// middleware
app.use(express.static('public'));

require('./config/session')(app);
require('./config/flash')(app);
require('./config/parser')(app);
require('./config/locals')(app);
require('./config/views')(app);

const db = require('./config/sql');

// routes
require('./routes/home.route')(app);
require('./routes/wonderful-copenhagen.route')(app);
require('./routes/barista-cup.route')(app);
require('./routes/sweden-2010.route')(app);
require('./routes/pagination.route')(app);



// port
app.listen(3000, () => {
    console.log('App is running on port 3000!');
})